public class AppController
{
    private AppModel model;
    private AppView view;
    
    public AppController(AppModel model, AppView view) {
        this.model = model;
        this.view = view;
        init();
    }
    
    public void setStatus(AppModel.State status) {
        if (status == AppModel.State.DB_CLOSED ) {
            view.getMRepository().setEnabled(false);
            view.getMiClose().setEnabled(false);
            view.getMiOpen().setEnabled(true);
            view.getMiNew().setEnabled(true);
        } else if (status == AppModel.State.DB_OPEN ) {
            view.getMRepository().setEnabled(true);
            view.getMiOpen().setEnabled(false);
            view.getMiNew().setEnabled(false);
            view.getMiClose().setEnabled(true);        
        }
    }
    
    public void init() {
        setStatus(AppModel.State.DB_CLOSED);
        view.getMiOpen().addActionListener(e -> fileOpen());
        view.getMiNew().addActionListener(e -> fileNew());
        view.getMiClose().addActionListener(e -> fileClose());  
        view.getMiPasswords().addActionListener(e -> openPasswordsRepository());
    }
    
    private void openDatabase() {   
        model.setFile(view.getFileChooser().getSelectedFile());
        view.getLStatus().setText("File: "+model.getFile().getPath());
        Database database = new Database(model.getFile().getPath());
        model.setDatabase(database);
        setStatus(AppModel.State.DB_OPEN);
    }
    
    public void fileOpen() {
        int returnVal = view.getFileChooser().showOpenDialog(view);
        if (returnVal == javax.swing.JFileChooser.APPROVE_OPTION) {
            openDatabase();
        } else {
            view.getLStatus().setText("Open file operation cancelled");
        }
    }
    
    public void fileNew() {
        int returnVal = view.getFileChooser().showSaveDialog(view);
        if (returnVal == javax.swing.JFileChooser.APPROVE_OPTION) {
            openDatabase();
        } else {
            view.getLStatus().setText("New file operation cancelled");
        }
    }
    
    public void fileClose() {
        model.getDatabase().close();
        setStatus(AppModel.State.DB_CLOSED);
        view.getTpRepository().removeAll();
    }
    
    public void openPasswordsRepository() {
        PasswordsRepositoryController passwordRepositoryController = PasswordsRepositoryController.createController(model.getDatabase());    
        view.getTpRepository().addTab("Passwords", passwordRepositoryController.getView());
    }
    
    public static void main(String[] args) {
        AppModel model = new AppModel();
        AppView view = new AppView();
        AppController ac = new AppController(model, view);
        view.setVisible(true);
    }
}
