import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.Dao;
import java.sql.SQLException;
import com.j256.ormlite.table.TableUtils;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.EnumSet;


public class PasswordsRepository{
    private static Database database;
    private static Dao<Passwords, Integer> dao;
    private List<Passwords> savedPasswords;
    private Passwords savedPassword; 
    private Passwords desiredPassword;
    
    public PasswordsRepository(Database database) {
        PasswordsRepository.setDatabase(database);
        savedPasswords = new ArrayList<Passwords>();
    }
    public static void setDatabase(Database database) {
        PasswordsRepository.database = database;
        try {
            dao = DaoManager.createDao(database.getConnection(), Passwords.class);
            TableUtils.createTableIfNotExists(database.getConnection(), Passwords.class);
        }
        catch(SQLException e) {
            System.out.println(e);
        }            
    }
    public Passwords create(Passwords password) {
        int nrows = 0;
        try {
            nrows = dao.create(password);
            if ( nrows == 0 )
                throw new SQLException("Error: object not saved");
            this.savedPassword = password;
            savedPasswords.add(password);
        } catch (SQLException e) {
            System.out.println(e);
        }
        return password;
    }
    public void update(Passwords password){
        this.savedPassword = password;
        if(this.savedPassword != null){
            
        }
        int nrows=0;
        try{
            nrows=dao.update(password);
            if(nrows==0){
                throw new SQLException("Error: password not updated");
            }
        }
        catch(SQLException e){
            System.out.println(e);
        } 
    }
    public void delete(Passwords password){
        int nrows=0;
        try{
            nrows=dao.delete(password);
            if(nrows==0){
                throw new SQLException("Error: password not updated");
            }
        }
        catch(SQLException e){
            System.out.println(e);
        } 
    }
    public Passwords loadFromDomain(String domain) {
        try {
            savedPasswords =  dao.queryForAll();
            if (savedPasswords.size() != 0){
                for (Passwords savedPassword : savedPasswords) {
                    if(savedPassword.getDomain().equals(domain)==true){
                        desiredPassword=savedPassword;
                    }
                }
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return desiredPassword;
    }
    public enum State { INITIAL, SEARCHING, CREATING, DELETING, 
    UPDATING, LOADED_ONE};
    public static Set<State> LOADED = EnumSet.of(State.LOADED_ONE);      
    private State state = State.INITIAL;
    public Database getDatabase(){
        return this.database;
    }
    public com.j256.ormlite.dao.Dao<Passwords, java.lang.Integer> getDao(){
        return this.dao;
    }
    public void setDao(com.j256.ormlite.dao.Dao<Passwords, java.lang.Integer> dao){
        this.dao = dao;
    }
    public State getState(){
        return this.state;
    }
    public void setState(State state){
        this.state = state;
    }
    public java.util.List<Passwords> getLoadedPasswords(){
        return this.savedPasswords;
    }
    public void setLoadedPasswords(java.util.List<Passwords> loadedPasswords){
        this.savedPasswords = loadedPasswords;
    }
    public Passwords getLoadedPassword(){
        return this.savedPassword;
    }
    public void setLoadedPassword(Passwords loadedPassword){
        this.savedPassword = loadedPassword;
    }

}