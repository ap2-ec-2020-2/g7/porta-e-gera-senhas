Este projeto tem por propósito a criação de uma ferramenta que gere e armazene senhas a partir de especificações dadas pelo usuário (número de caracteres, a presença ou não de caracteres especiais e letras maiúsculas e minúsculas).
Para isso haverá uma interface tanto para a seleção das especificações quanto para a atribuição de nomes para a identificação pelo usuário. A exemplo o usuário atribuiria o nome de Facebook para uma senha que ele gerasse com o propósito de uso de uma conta no Facebook.

Grupo:

Thiago dos Santos Machado - @ThiagodsMachado
Matheus Carlos Lima e Silva - @matheuscarlos
Victor Shin Iti Kanazawa Noda - @victornoda
Brener Pereira de Araújo - @breneraraujo
Miguel Leonardo de Araujo Almeida - @miguel_almeida
