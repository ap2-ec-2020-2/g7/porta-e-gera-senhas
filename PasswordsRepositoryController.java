import javax.swing.JButton;
import java.util.List;
import javax.swing.JComponent;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;
import javax.swing.JCheckBox;
import java.util.Random;

public class PasswordsRepositoryController {
    private PasswordRepositoryView view;
    private PasswordsRepository model;

  public PasswordsRepositoryController(PasswordsRepository model, PasswordRepositoryView view)
  {
      this.model = model;
      this.view = view;
      init();
  }
  public void init() {
      model.setState(PasswordsRepository.State.INITIAL);
      view.getButtonFormPanel().getBCreate().addActionListener(e -> creating());
      view.getButtonFormPanel().getBSearch().addActionListener(e -> searching());
      view.getButtonFormPanel().getBOk().addActionListener(e -> commit());
      view.getButtonFormPanel().getBCancel().addActionListener(e -> cancel());
      updateView();
  }
  private void enableComponents(List<JComponent> components) {
      for (JComponent component : components)
          component.setEnabled(true);
  }
  private void disableComponents(List<JComponent> components) {
      for (JComponent component : components)
          component.setEnabled(false);
  } 
  private void clearFields(List<JComponent> components) {
      for (JComponent component: components)
          ((javax.swing.JTextField) component).setText("");
  }
  public void updateView() {
    if (model.getState() == PasswordsRepository.State.INITIAL ) {
        disableComponents(view.getButtonFormPanel().getOkCancelButtons());
        disableComponents(view.getButtonFormPanel().getDeleteUpdateButttons());
        enableComponents(view.getButtonFormPanel().getSearchCreateButttons());
        clearFields(view.getPasswordFormPanel().getAllFields());
        disableComponents(view.getPasswordFormPanel().getAllFields());
        disableComponents(view.getPasswordFormPanel().getAllCb());
    }
    else if (model.getState() == PasswordsRepository.State.CREATING){
        enableComponents(view.getButtonFormPanel().getOkCancelButtons());
        disableComponents(view.getButtonFormPanel().getDeleteUpdateButttons());
        disableComponents(view.getButtonFormPanel().getSearchCreateButttons());
        clearFields(view.getPasswordFormPanel().getAllFields());
        enableComponents(view.getPasswordFormPanel().getAllFields()); 
        enableComponents(view.getPasswordFormPanel().getAllCb());
    }
    else if ( model.getState() == PasswordsRepository.State.SEARCHING ){
        enableComponents(view.getButtonFormPanel().getOkCancelButtons());
        disableComponents(view.getButtonFormPanel().getDeleteUpdateButttons());
        disableComponents(view.getButtonFormPanel().getSearchCreateButttons());
        clearFields(view.getPasswordFormPanel().getAllFields());
        disableComponents(view.getPasswordFormPanel().getAllCb());
        view.getPasswordFormPanel().getTfDomain().setEnabled(true);
    }
    else if ( model.getState() == PasswordsRepository.State.LOADED_ONE ){
        enableComponents(view.getButtonFormPanel().getOkCancelButtons());
        enableComponents(view.getButtonFormPanel().getDeleteUpdateButttons());
        disableComponents(view.getButtonFormPanel().getSearchCreateButttons());
        disableComponents(view.getPasswordFormPanel().getAllCb());
    }
  }
  public void creating() {
      model.setState(PasswordsRepository.State.CREATING);       
      updateView();
    }
  public void searching() {
      model.setState(PasswordsRepository.State.SEARCHING);
      updateView();
  }
  public static PasswordsRepositoryController createController(Database database) {
        PasswordRepositoryView view = new PasswordRepositoryView();
        PasswordsRepository model = new PasswordsRepository(database);
        PasswordsRepositoryController controller = new PasswordsRepositoryController(model, view);
        return controller;
  }
  private int stringToInt( String string ) {
        return string.equals("") ? 0 : Integer.parseInt(string);
  }
  private Passwords loadFromView() throws Exception {
        Passwords passwordLoaded = new Passwords();
        String strLength = view.getPasswordFormPanel().getTfLength().getText();
        int length = stringToInt(strLength);
        passwordLoaded.setDomain(view.getPasswordFormPanel().getTfDomain().getText());
        return passwordLoaded;
  }
  public void commit() {
        if ( model.getState() == PasswordsRepository.State.CREATING ) {
            try {
                Passwords PasswordLoaded = loadFromView();
                Passwords newPassword = model.create(PasswordLoaded);
                String strLength = view.getPasswordFormPanel().getTfLength().getText();
                int length = stringToInt(strLength);
                char[] password = passw(length);
                newPassword.setPassword(String.valueOf(password));
                model.update(newPassword);
                JOptionPane.showMessageDialog(view, "Password " +newPassword.getPassword() +" created for " + newPassword.getDomain());
                model.setState(PasswordsRepository.State.INITIAL);
                updateView();
            } catch(Exception e) {
                JOptionPane.showMessageDialog(view,"Error: Details: "+e);
            } 
        } else if ( model.getState() == PasswordsRepository.State.SEARCHING ) {
              try {
                  Passwords passwordFound = model.loadFromDomain(view.getPasswordFormPanel().getTfDomain().getText());
                  if ( passwordFound != null ) {
                      model.setState(PasswordsRepository.State.LOADED_ONE);
                      JOptionPane.showMessageDialog(view, "Password "+passwordFound.getPassword());
                      model.setState(PasswordsRepository.State.INITIAL);
                  }
                  else{
                          JOptionPane.showMessageDialog(view, "Password not found");
                  }
                  updateView();
              } catch (Exception e) {
                  JOptionPane.showMessageDialog(view, "Error: Searching not performed. Details: "+e);
              }
        }     
    }
    public void cancel() {
        if ( model.getState() == PasswordsRepository.State.CREATING ) {
            model.setState(PasswordsRepository.State.INITIAL);
            updateView();
        }
        if ( model.getState() == PasswordsRepository.State.SEARCHING ) {
            model.setState(PasswordsRepository.State.INITIAL);
            updateView();
        }
    }
    public PasswordRepositoryView getView(){
        return this.view;
    }
    public void setView(PasswordRepositoryView view){
        this.view = view;
    }
    public PasswordsRepository getModel(){
        return this.model;
    }
    public void setModel(PasswordsRepository model){
        this.model = model;
    }
    public static void main(String[] args) {
        PasswordsRepository model = new PasswordsRepository(new Database(""));
        PasswordRepositoryView view = new PasswordRepositoryView();
        PasswordsRepositoryController passwordRepositoryController = new PasswordsRepositoryController(model, view);
        javax.swing.JFrame jFrame = new javax.swing.JFrame();
        jFrame.setSize(570,320);
        jFrame.setLayout(new java.awt.BorderLayout());
        jFrame.add(view, java.awt.BorderLayout.CENTER);
        jFrame.setVisible(true);
    }
    
    public char[] passw(int char_numbers){
        int n = 0;
        boolean letter1 = view.getPasswordFormPanel().getCbLetter().isSelected();
        if(view.getPasswordFormPanel().getCbNumber().isSelected() == true){
            n+=10;
        }
        if(view.getPasswordFormPanel().getCbLetter().isSelected() == true){
            n+=26;
        }
        if(view.getPasswordFormPanel().getCbUpper().isSelected() == true){
            n+=26;
        }
        if(view.getPasswordFormPanel().getCbSpecial().isSelected() == true){
            n+=31;
        }
        char[] passw = new char[char_numbers];
        int[] character = new int[char_numbers];
        for(int i=0; i<char_numbers; i++){
            Random rand = new Random();
            character[i] = rand.nextInt(n-1);
        }
        for(int i=0; i<char_numbers; i++){
            if(n==10){
                passw[i] = (char)('0' + character[i]);
            }
            else if(n==26){
                if(letter1=true){
                    passw[i] = (char)('a' + character[i]);
                }
                else{
                    passw[i] = (char)('A' + character[i]);
                }
            }
            else if(n==31){
                    if(character[i]<=13){
                        passw[i] = (char)('!' + character[i]);
                    }
                    if(character[i]>13 && character[i]<=20){
                        passw[i] = (char)(':' + character[i] - 14);
                    }
                    if(character[i]>20 && character[i]<=26){
                        passw[i] = (char)('[' + character[i] - 21);
                    }
                    if(character[i]>26 && character[i]<=30){
                        passw[i] = (char)('{' + character[i] - 27);
                    }
            }
            else if(n==36){
                if(letter1==true){
                    if(character[i] < 10){    
                        passw[i] = (char)('0' + character[i]);
                    }
                    if(character[i] >= 10){
                        passw[i] = (char)('a' + character[i] -10);
                    }
                }
                else{
                    if(character[i] < 10){    
                        passw[i] = (char)('0' + character[i]);
                    }
                    if(character[i] >= 10){
                        passw[i] = (char)('A' + character[i] -10);
                    }
                }
            }
            else if(n==41){
                if(character[i] < 10){    
                    passw[i] = (char)('0' + character[i]);
                }
                if(character[i] >= 10){ 
                    if(character[i]<=23){
                        passw[i] = (char)('!' + character[i] - 10);
                    }
                    if(character[i]>23 && character[i]<=30){
                        passw[i] = (char)(':' + character[i] - 24);
                    }
                    if(character[i]>30 && character[i]<=36){
                        passw[i] = (char)('[' + character[i] - 31);
                    }
                    if(character[i]>36 && character[i]<=40){
                        passw[i] = (char)('{' + character[i] - 37);
                    }
                }
            }
            else if(n==52){
                if(character[i]<=25){
                    passw[i] = (char)('a' + character[i]);
                }
                else{
                    passw[i] = (char)('A' + character[i] - 26);
                }
            }
            else if(n==57){
                if(letter1=true){
                    if(character[i]<=25){
                        passw[i] = (char)('a' + character[i]);
                    }
                    if(character[i]>25){
                        if(character[i]<=39){
                            passw[i] = (char)('!' + character[i] - 26);
                        }
                        if(character[i]>39 && character[i]<=46){
                            passw[i] = (char)(':' + character[i] - 40);
                        }
                        if(character[i]>46 && character[i]<=52){
                            passw[i] = (char)('[' + character[i] - 47);
                        }   
                        if(character[i]>52 && character[i]<=56){
                            passw[i] = (char)('{' + character[i] - 53);
                        }
                    }
                }
                else{
                    if(character[i]<=25){
                        passw[i] = (char)('A' + character[i]);
                    }
                    if(character[i]>25){
                        if(character[i]<=39){
                            passw[i] = (char)('!' + character[i] - 26);
                        }
                        if(character[i]>39 && character[i]<=46){
                            passw[i] = (char)(':' + character[i] - 40);
                        }
                        if(character[i]>46 && character[i]<=52){
                            passw[i] = (char)('[' + character[i] - 47);
                        }   
                        if(character[i]>52 && character[i]<=56){
                            passw[i] = (char)('{' + character[i] - 53);
                        }
                    }
                }
            }
            else{
                if(character[i] < 10){    
                    passw[i] = (char)('0' + character[i]);
                }
                if(character[i] >= 10 && character[i]<=35){
                    passw[i] = (char)('a' + character[i] -10);
                }
                if(character[i] >= 36 && character[i]<=61){
                    passw[i] = (char)('A' + character[i] -36);
                }
                if(character[i]>62){
                    if(character[i]<=76){
                        passw[i] = (char)('!' + character[i] - 63);
                    }
                    if(character[i]>76 && character[i]<=85){
                        passw[i] = (char)(':' + character[i] - 77);
                    }
                    if(character[i]>85 && character[i]<=91){
                        passw[i] = (char)('[' + character[i] - 86);
                    }   
                    if(character[i]>91 && character[i]<=95){
                        passw[i] = (char)('{' + character[i] - 92);
                    }
                }
            }
        }
        return passw;
}









}
