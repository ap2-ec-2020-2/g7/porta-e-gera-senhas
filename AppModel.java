import java.io.File;

public class AppModel
{
    private File file;
    private Database database;
    public enum State { DB_CLOSED, DB_OPEN }
    private State state;

    public File getFile(){
        return this.file;
    }

    public void setFile(File file){
        this.file = file;
    }

    public Database getDatabase(){
        return this.database;
    }

    public void setDatabase(Database database){
        this.database = database;
    }

    public State getState(){
        return this.state;
    }

    public void setState(State state){
        this.state = state;
    }

}
