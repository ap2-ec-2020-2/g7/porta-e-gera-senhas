import javax.swing.JMenuBar;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JFileChooser;
import javax.swing.JTabbedPane; 


public class AppView extends javax.swing.JFrame {

    public AppView() {
        initComponents();
    }                       
    private void initComponents() {

        FileChooser = new javax.swing.JFileChooser();
        pStatus = new javax.swing.JPanel();
        lStatus = new javax.swing.JLabel();
        tpRepository = new javax.swing.JTabbedPane();
        jMenuBar1 = new javax.swing.JMenuBar();
        mFile = new javax.swing.JMenu();
        miNew = new javax.swing.JMenuItem();
        miOpen = new javax.swing.JMenuItem();
        miClose = new javax.swing.JMenuItem();
        mRepository = new javax.swing.JMenu();
        miPasswords = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setSize(new java.awt.Dimension(580, 400));

        pStatus.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        lStatus.setText("File: not opened");
        pStatus.add(lStatus);

        getContentPane().add(pStatus, java.awt.BorderLayout.PAGE_END);
        getContentPane().add(tpRepository, java.awt.BorderLayout.CENTER);

        mFile.setText("File");

        miNew.setText("New");
        mFile.add(miNew);

        miOpen.setText("Open");
        mFile.add(miOpen);

        miClose.setText("Close");
        mFile.add(miClose);

        jMenuBar1.add(mFile);

        mRepository.setText("Repository");

        miPasswords.setText("Passwords");
        mRepository.add(miPasswords);

        jMenuBar1.add(mRepository);

        setJMenuBar(jMenuBar1);

        // pack();
    }                        

   
    public static void main(String args[]) {
        
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(AppView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AppView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AppView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(AppView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new AppView().setVisible(true);
            }
        });
    }
                   
    private javax.swing.JFileChooser FileChooser;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JLabel lStatus;
    private javax.swing.JMenu mFile;
    private javax.swing.JMenu mRepository;
    private javax.swing.JMenuItem miClose;
    private javax.swing.JMenuItem miNew;
    private javax.swing.JMenuItem miOpen;
    private javax.swing.JMenuItem miPasswords;
    private javax.swing.JPanel pStatus;
    private javax.swing.JTabbedPane tpRepository;

    public JFileChooser getFileChooser(){
        return this.FileChooser;
    }

    public void setFileChooser(JFileChooser FileChooser){
        this.FileChooser = FileChooser;
    }

    public JMenuBar getJMenuBar1(){
        return this.jMenuBar1;
    }

    public void setJMenuBar1(JMenuBar jMenuBar1){
        this.jMenuBar1 = jMenuBar1;
    }

    public JLabel getLStatus(){
        return this.lStatus;
    }

    public void setLStatus(JLabel lStatus){
        this.lStatus = lStatus;
    }

    public JMenu getMFile(){
        return this.mFile;
    }

    public void setMFile(JMenu mFile){
        this.mFile = mFile;
    }

    public JMenu getMRepository(){
        return this.mRepository;
    }

    public void setMRepository(JMenu mRepository){
        this.mRepository = mRepository;
    }

    public JMenuItem getMiClose(){
        return this.miClose;
    }

    public void setMiClose(JMenuItem miClose){
        this.miClose = miClose;
    }

    public JMenuItem getMiNew(){
        return this.miNew;
    }

    public void setMiNew(JMenuItem miNew){
        this.miNew = miNew;
    }

    public JMenuItem getMiOpen(){
        return this.miOpen;
    }

    public void setMiOpen(JMenuItem miOpen){
        this.miOpen = miOpen;
    }

    
    public JMenuItem getMiPasswords(){
        return this.miPasswords;
    }

    public void setMiPasswords(JMenuItem miPasswords){
        this.miPasswords = miPasswords;
    }

    public JPanel getPStatus(){
        return this.pStatus;
    }

    public void setPStatus(JPanel pStatus){
        this.pStatus = pStatus;
    }

    public JTabbedPane getTpRepository(){
        return this.tpRepository;
    }

    public void setTpRepository(JTabbedPane tpRepository){
        this.tpRepository = tpRepository;
    }

}

