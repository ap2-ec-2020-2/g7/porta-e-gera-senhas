import com.j256.ormlite.table.DatabaseTable;
import com.j256.ormlite.field.DatabaseField;

@DatabaseTable(tableName = "password")
public class Passwords
{  
    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField
    private String password;
    
    @DatabaseField
    public String domain;
    
    public int getId(){
        return this.id;
    }
     public void setId(int id){
        this.id = id;
    }
    public String getPassword(){
        return this.password;
    }
    public void setPassword(String password){
        this.password = password;
    }
    public String getDomain(){
        return this.domain;
    }
    public void setDomain(String domain){
        this.domain = domain;
    }
}
